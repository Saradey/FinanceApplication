package financeapplication.android8.sarad.financeapplication;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.User;
import financeapplication.android8.sarad.financeapplication.FunctionalDb.NoteDataSource;
import financeapplication.android8.sarad.financeapplication.MyFragment.FragmentAllInformation;
import financeapplication.android8.sarad.financeapplication.MyFragment.FragmentMain;
import financeapplication.android8.sarad.financeapplication.functional.SingletonUser;

public class ActivityNavigationDrawer extends AppCompatActivity {

    private static final String PATTERN_DATE = "MMM yyyy";
    private static final String SAVE_USER = "get_user";
    private static final String GET_ID = "get_id";
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    NoteDataSource noteDataSource;
    FragmentMain fragmentMain;

    @BindString(R.string.position_draw_text)
    String POSITIVE_BUTTON_TEXT;

    @BindString(R.string.negativ_draw_text)
    String NEGATIVE_BUTTON_TEXT;
    private int index = -1; //для смены списков

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);

        ButterKnife.bind(this);

        initUI();
        loadSettings();
    }


    private void loadSettings() {

        int id_user = 0;
        SharedPreferences sharedPreferences = getSharedPreferences(SAVE_USER, Context.MODE_PRIVATE);
        if (sharedPreferences.contains(GET_ID)) {
            id_user = sharedPreferences.getInt(GET_ID, -1);
            SingletonUser.getInstance().setId_user(id_user);
        } else {
            DialogWindowAddUser();
        }

    }

    public void DialogWindowAddUser() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final TextView textView = new TextView(this);
        textView.setText(getString(R.string.user_title_2));
        layout.addView(textView);

        final EditText manyBox = new EditText(this);
        manyBox.setHint(getString(R.string.user_title));
        layout.addView(manyBox);

        dialog.setView(layout);
        dialog.setNegativeButton(R.string.negativ_draw_text, null);

        dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                User entity = new User();
                Random random = new Random();
                int user_id_teamp = random.nextInt();

                try {
                    double price = Double.parseDouble(manyBox.getText().toString());
                    price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();

                    if (price > 0) {
                        entity.setId(user_id_teamp);
                        entity.setAllMoney(price);
                        noteDataSource.addUser(entity);

                        SingletonUser.getInstance().setId_user(user_id_teamp);
                        SingletonUser.getInstance().setAllMoney(price);

                        SharedPreferences sharedPreferences = getSharedPreferences(SAVE_USER, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt(GET_ID, user_id_teamp);
                        editor.apply();
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }

    //иницилизация ui
    private void initUI() {
        initToolbarAndFloatingActionButton();
        initDrawerLayoutAndNavigationLayout();
        initGradient();
        initDataBase();
        drawer.openDrawer(GravityCompat.START);
    }

    //иницилизируем базу данных
    public void initDataBase() {
        noteDataSource = new NoteDataSource(getApplicationContext());
    }


    public NoteDataSource getNoteDataSource() {
        return noteDataSource;
    }

    //создаем градиент
    private void initGradient() {
        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.BR_TL,
                new int[]{ContextCompat.getColor(this, R.color.myGradient2),
                        ContextCompat.getColor(this, R.color.myGradient)});
        gradientDrawable.setCornerRadius(0f);
        drawer.setBackground(gradientDrawable);
    }

    //иницилизируем
    private void initToolbarAndFloatingActionButton() {
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(getClickListener());
    }

    //иницилизация невегейшен драйвера
    private void initDrawerLayoutAndNavigationLayout() {
        // ActionBarDrawerToggle toggle объядиняет в себе дрвайвер лейаут и тулбар помогает определить как навегейт
        //дрвайвер будет наезжать на тулбар и активити
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(getListener());

    }

    //нажатие для navigationView
    @NonNull
    private NavigationView.OnNavigationItemSelectedListener getListener() {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_item1:
                        pressItem1();
                        break;

                    case R.id.nav_item2:
                        pressItem2();
                        break;

                    case R.id.nav_item3:
                        pressItem3();
                        break;

                    case R.id.nav_item4:
                        pressItem4();
                        break;

                    case R.id.nav_item5:
                        pressItem5();
                        break;

                    case R.id.nav_item6:
                        pressItem6();
                        break;

                    case R.id.nav_item7:
                        pressItem7();
                        break;

                    case R.id.nav_item8:
                        pressItem8();
                        break;

                    case R.id.nav_item9:
                        goToTheFragmentAllInformation();
                        break;

                    case R.id.nav_send:

                        break;
                }
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        };
    }


    private void goToTheFragmentAllInformation() {
        if (fragmentMain != null) fragmentMain = null;
        FragmentAllInformation fragmentAllInformation = new FragmentAllInformation();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.fragment_base, fragmentAllInformation);
        transaction.commit();
    }


    public String getDateNow() {
        SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_DATE);
        Date currentDate = new Date();
        return " " + sdf.format(currentDate);
    }


    private void pressItem8() {
        String date = getString(R.string.consumption_now) + getDateNow();
        setTitle(date);
        index = 8;
        goToTheMainFragment(index);
    }


    private void pressItem7() {
        String date = getString(R.string.income_now) + getDateNow();
        setTitle(date);
        index = 7;
        goToTheMainFragment(index);
    }


    private void pressItem6() {
        String date = getString(R.string.planned_flow) + getDateNow();
        setTitle(date);
        index = 6;
        goToTheMainFragment(index);
    }


    private void pressItem5() {
        String date = getString(R.string.postponing) + getDateNow();
        setTitle(date);
        index = 5;
        goToTheMainFragment(index);
    }


    private void pressItem4() {
        String date = getString(R.string.income) + getDateNow();
        setTitle(date);
        index = 4;
        goToTheMainFragment(index);
    }


    private void pressItem3() {
        String date = getString(R.string.consumption) + getDateNow();
        setTitle(date);
        index = 3;
        goToTheMainFragment(index);
    }


    private void pressItem2() {
        String date = getString(R.string.reminders) + getDateNow();
        setTitle(date);
        index = 2;
        goToTheMainFragment(index);
    }


    private void pressItem1() {
        String date = getString(R.string.list_meetings) + getDateNow();
        setTitle(date);
        index = 1;
        goToTheMainFragment(index);
    }


    //создаем фрагмент для отображения
    private void goToTheMainFragment(int index) {
        if (fragmentMain != null) fragmentMain = null;
        fragmentMain = new FragmentMain();
        fragmentMain.setDataSource(noteDataSource);
        fragmentMain.setIndexDatabase(index);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.fragment_base, fragmentMain);
        transaction.commit();
    }


    //селект для тулбара
    @NonNull
    private View.OnClickListener getClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragmentMain != null) {
                    switch (index) {
                        case 1:
                            fragmentMain.DialogWindowAddMeetings(-1);
                            break;

                        case 2:
                            fragmentMain.DialogWindowAddReminders(-1);
                            break;

                        case 3:
                            fragmentMain.DialogWindowAddConsumption(-1);
                            break;

                        case 4:
                            fragmentMain.DialogWindowAddIncome(-1);
                            break;

                        case 5:
                            fragmentMain.DialogWindowAddPostponing(-1);
                            break;

                        case 6:
                            fragmentMain.DialogWindowAddPlannedFlow(-1);
                            break;

                        case 7:
                            fragmentMain.DialogWindowAddIncomeNowEntity(-1);
                            break;

                        case 8:
                            fragmentMain.DialogWindowAddConsumptionNowEntity(-1);
                            break;

                        default:
                            break;
                    }
                }
            }
        };
    }


    //если нажали кнопку назад
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    //создем верхние меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    //если нажаоли на верхние меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        showToast("верхние меню");
        return super.onOptionsItemSelected(item);
    }


    private void showToast(String str) {
        Toast toast = Toast.makeText(getApplicationContext(),
                str, Toast.LENGTH_SHORT);
        toast.show();
    }

}