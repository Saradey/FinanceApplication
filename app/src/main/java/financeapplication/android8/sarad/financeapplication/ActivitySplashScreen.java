package financeapplication.android8.sarad.financeapplication;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySplashScreen extends AppCompatActivity {

    protected int _splashTime = 2000;
    @BindView(R.id.start_splash_screen)
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ButterKnife.bind(this);
        initGradient();
        startWaitThread();
    }


    private void initGradient() {
        getSupportActionBar().hide();
        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.BR_TL,
                new int[]{ContextCompat.getColor(this, R.color.myGradient2)
                        , ContextCompat.getColor(this, R.color.myGradient)});
        gradientDrawable.setCornerRadius(0f);
        relativeLayout.setBackground(gradientDrawable);
    }


    private void startWaitThread() {

        new Thread() {

            @Override
            public void run() {

                try {
                    synchronized (this) {
                        wait(_splashTime);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent i = new Intent();
                    i.setClass(ActivitySplashScreen.this, ActivityNavigationDrawer.class);
                    startActivity(i);
                    finish();
                }
            }

        }.start();

    }


}
