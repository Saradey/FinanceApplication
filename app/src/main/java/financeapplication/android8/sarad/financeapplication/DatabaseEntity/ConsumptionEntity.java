package financeapplication.android8.sarad.financeapplication.DatabaseEntity;


public class ConsumptionEntity implements Entity {


    private static final String NAME = "Название постоянного ежемесячного расхода: ";
    private static final String PRICE = "Цена: ";
    private int id_Consumption;
    private String name_Consumption;
    private Double price;
    private int _id_user;

    public int getId_Consumption() {
        return id_Consumption;
    }

    public void setId_Consumption(int id_Consumption) {
        this.id_Consumption = id_Consumption;
    }

    public String getName_Consumption() {
        return name_Consumption;
    }

    public void setName_Consumption(String name_Consumption) {
        this.name_Consumption = name_Consumption;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int get_id_user() {
        return _id_user;
    }

    public void set_id_user(int _id_user) {
        this._id_user = _id_user;
    }

    @Override
    public int getId() {
        return id_Consumption;
    }

    @Override
    public void setId(int id) {
        id_Consumption = id;
    }


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(NAME);
        stringBuilder.append(name_Consumption);
        stringBuilder.append("\n\n");
        stringBuilder.append(PRICE);
        stringBuilder.append(price);
        return stringBuilder.toString();
    }
}
