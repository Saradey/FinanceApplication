package financeapplication.android8.sarad.financeapplication.DatabaseEntity;


public class ConsumptionNowEntity implements Entity {

    private static final String NAME = "Название расхода: ";
    private static final String PRICE = "Стоимость расхода:";
    private static final String DATE = "Дата: ";
    private int id_consumption;
    private String _id_months;
    private String name_consumptione;
    private Double price;
    private String date_consumption;

    public static String getNAME() {
        return NAME;
    }

    public static String getPRICE() {
        return PRICE;
    }

    public static String getDATE() {
        return DATE;
    }

    public String get_id_months() {
        return _id_months;
    }

    public void set_id_months(String _id_months) {
        this._id_months = _id_months;
    }

    public String getName_consumptione() {
        return name_consumptione;
    }

    public void setName_consumptione(String name_consumptione) {
        this.name_consumptione = name_consumptione;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDate_consumption() {
        return date_consumption;
    }

    public void setDate_consumption(String date_consumption) {
        this.date_consumption = date_consumption;
    }

    @Override
    public int getId() {
        return id_consumption;
    }

    @Override
    public void setId(int id) {
        id_consumption = id;
    }


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(NAME);
        stringBuilder.append(name_consumptione);
        stringBuilder.append("\n\n");
        stringBuilder.append(PRICE);
        stringBuilder.append(price);
        stringBuilder.append("\n\n");
        stringBuilder.append(DATE);
        stringBuilder.append(date_consumption);
        return stringBuilder.toString();
    }


}
