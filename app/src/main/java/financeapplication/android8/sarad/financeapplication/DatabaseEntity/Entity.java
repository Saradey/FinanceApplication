package financeapplication.android8.sarad.financeapplication.DatabaseEntity;


public interface Entity {

    int getId();

    void setId(int id);

}
