package financeapplication.android8.sarad.financeapplication.DatabaseEntity;


public class IncomeEntity implements Entity {

    private static final String NAME = "Название постоянного ежемесячного дохода: ";
    private static final String PRICE = "Цена: ";
    private int id_Income;
    private String name_Income;
    private Double price;

    private int _id_user;

    public String getName_Income() {
        return name_Income;
    }

    public void setName_Income(String name_Income) {
        this.name_Income = name_Income;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int get_id_user() {
        return _id_user;
    }

    public void set_id_user(int _id_user) {
        this._id_user = _id_user;
    }

    @Override
    public int getId() {
        return id_Income;
    }

    @Override
    public void setId(int id) {
        id_Income = id;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(NAME);
        stringBuilder.append(name_Income);
        stringBuilder.append("\n\n");
        stringBuilder.append(PRICE);
        stringBuilder.append(price);
        return stringBuilder.toString();
    }
}
