package financeapplication.android8.sarad.financeapplication.DatabaseEntity;

public class IncomeNowEntity implements Entity {

    private static final String NAME = "Название дохода: ";
    private static final String PRICE = "Стоимость дохода: ";
    private static final String DATE = "Дата: ";
    private int id_Income;
    private String _id_months;
    private String name_Income;
    private Double price;
    private String _date;

    public static String getNAME() {
        return NAME;
    }

    public static String getPRICE() {
        return PRICE;
    }

    public static String getDATE() {
        return DATE;
    }

    public String get_id_months() {
        return _id_months;
    }

    public void set_id_months(String _id_months) {
        this._id_months = _id_months;
    }

    public String getName_Income() {
        return name_Income;
    }

    public void setName_Income(String name_Income) {
        this.name_Income = name_Income;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String get_date() {
        return _date;
    }

    public void set_date(String _date) {
        this._date = _date;
    }

    @Override
    public int getId() {
        return id_Income;
    }

    @Override
    public void setId(int id) {
        id_Income = id;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(NAME);
        stringBuilder.append(name_Income);
        stringBuilder.append("\n\n");
        stringBuilder.append(PRICE);
        stringBuilder.append(price);
        stringBuilder.append("\n\n");
        stringBuilder.append(DATE);
        stringBuilder.append(_date);
        return stringBuilder.toString();
    }


}
