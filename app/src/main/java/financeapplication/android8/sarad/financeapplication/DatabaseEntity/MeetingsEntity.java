package financeapplication.android8.sarad.financeapplication.DatabaseEntity;


public class MeetingsEntity implements Entity {

    private static final String NAME_MEETINGS = "Название встречи: ";
    private static final String DATE_MEETINGS = "Дата встречи: ";
    private int id_meetings;
    private String name_meetings;
    private String date_meetings;

    public int getId_meetings() {
        return id_meetings;
    }

    public void setId_meetings(int id_meetings) {
        this.id_meetings = id_meetings;
    }

    public String getName_meetings() {
        return name_meetings;
    }

    public void setName_meetings(String name_meetings) {
        this.name_meetings = name_meetings;
    }

    public String getDate_meetings() {
        return date_meetings;
    }

    public void setDate_meetings(String date_meetings) {
        this.date_meetings = date_meetings;
    }

    @Override
    public int getId() {
        return id_meetings;
    }

    @Override
    public void setId(int id) {
        id_meetings = id;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(NAME_MEETINGS);
        stringBuilder.append(name_meetings);
        stringBuilder.append("\n\n");
        stringBuilder.append(DATE_MEETINGS);
        stringBuilder.append(date_meetings);
        return stringBuilder.toString();
    }
}
