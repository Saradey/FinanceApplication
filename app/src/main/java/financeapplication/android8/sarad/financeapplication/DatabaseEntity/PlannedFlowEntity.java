package financeapplication.android8.sarad.financeapplication.DatabaseEntity;


public class PlannedFlowEntity implements Entity {

    private static final String NAME = "Название планируемого расхода: ";
    private static final String PRICE = "Стоимость планируемого расхода: ";
    private int id_planned_flow;
    private String _id_months;
    private String name_planned_flow;
    private Double price;

    public String get_id_months() {
        return _id_months;
    }

    public void set_id_months(String _id_months) {
        this._id_months = _id_months;
    }

    public String getNAME() {
        return NAME;
    }

    public String getName_planned_flow() {
        return name_planned_flow;
    }

    public void setName_planned_flow(String name_planned_flow) {
        this.name_planned_flow = name_planned_flow;
    }

    public Double getPRICE() {
        return price;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public int getId() {
        return id_planned_flow;
    }

    @Override
    public void setId(int id) {
        id_planned_flow = id;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(NAME);
        stringBuilder.append(name_planned_flow);
        stringBuilder.append("\n\n");
        stringBuilder.append(PRICE);
        stringBuilder.append(price);
        return stringBuilder.toString();
    }
}
