package financeapplication.android8.sarad.financeapplication.DatabaseEntity;

import java.text.DecimalFormat;

public class PostponingEntity implements Entity {

    private static final String VALUE = "Стоимость цели: ";
    private static final String MANY_MANTHS = "Сколько месяцев откладывать: ";
    private static final String NAME = "Название цели: ";
    private static final String RESULT = "Сколько денег отложить в этом месяце:";
    private int id_Postponing;
    private Double value_of_a_goal;
    private int many_months;
    private String name_postponing;
    private Double result;

    public void calculatingPostponing() {
        result = value_of_a_goal / many_months;
        DecimalFormat newFormat = new DecimalFormat("#.##");
        result = Double.valueOf(newFormat.format(result));
    }

    public Double getResult() {
        return result;
    }

    public Double getValue_of_a_goal() {
        return value_of_a_goal;
    }

    public void setValue_of_a_goal(Double value_of_a_goal) {
        this.value_of_a_goal = value_of_a_goal;
    }

    public int getMany_months() {
        return many_months;
    }

    public void setMany_months(int many_months) {
        this.many_months = many_months;
    }

    public String getName_postponing() {
        return name_postponing;
    }

    public void setName_postponing(String name_postponing) {
        this.name_postponing = name_postponing;
    }

    @Override
    public int getId() {
        return id_Postponing;
    }

    @Override
    public void setId(int id) {
        id_Postponing = id;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(NAME);
        stringBuilder.append(name_postponing);
        stringBuilder.append("\n\n");
        stringBuilder.append(VALUE);
        stringBuilder.append(value_of_a_goal);
        stringBuilder.append("\n\n");
        stringBuilder.append(MANY_MANTHS);
        stringBuilder.append(many_months);
        stringBuilder.append("\n\n");
        stringBuilder.append(RESULT);
        stringBuilder.append(result);
        return stringBuilder.toString();
    }
}
