package financeapplication.android8.sarad.financeapplication.DatabaseEntity;

public class RemindersEntity implements Entity {

    private static final String NAME = "Название напоминания: ";
    private static final String URGENCY = "Срочность: ";
    private int id_reminders;
    private String name_reminders;
    private String urgency;
    private int _id_user;

    public String getName_reminders() {
        return name_reminders;
    }

    public void setName_reminders(String name_reminders) {
        this.name_reminders = name_reminders;
    }

    public String getUrgency() {
        return urgency;
    }

    public void setUrgency(String urgency) {
        this.urgency = urgency;
    }

    public int get_id_user() {
        return _id_user;
    }

    public void set_id_user(int _id_user) {
        this._id_user = _id_user;
    }

    @Override
    public int getId() {
        return id_reminders;
    }

    @Override
    public void setId(int id) {
        id_reminders = id;
    }


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(NAME);
        stringBuilder.append(name_reminders);
        stringBuilder.append("\n\n");
        stringBuilder.append(URGENCY);
        stringBuilder.append(urgency);
        return stringBuilder.toString();
    }
}
