package financeapplication.android8.sarad.financeapplication.DatabaseEntity;

public class User implements Entity {

    private static final String ID_USER = "id пользователя: ";
    private int id_user;
    private Double allMoney;

    public Double getAllMoney() {
        return allMoney;
    }

    public void setAllMoney(Double allMoney) {
        this.allMoney = allMoney;
    }

    @Override
    public int getId() {
        return id_user;
    }

    @Override
    public void setId(int id) {
        id_user = id;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(ID_USER);
        stringBuilder.append(id_user);
        return stringBuilder.toString();
    }
}
