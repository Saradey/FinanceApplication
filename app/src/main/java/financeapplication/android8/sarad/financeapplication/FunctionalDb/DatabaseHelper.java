package financeapplication.android8.sarad.financeapplication.FunctionalDb;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import financeapplication.android8.sarad.financeapplication.Table.MyTable;


public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "myFinance.db";

    private MyTable myTable = new MyTable();


    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            //делаем юзера
            db.execSQL("CREATE TABLE "
                    + myTable.getUser().getNameTable()
                    + " ("
                    + myTable.getUser().getColumnId()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + myTable.getUser().getAllMoney()
                    + " REAL"
                    + ");");

            //делаем встречи
            db.execSQL("CREATE TABLE "
                    + myTable.getMeetings().getNameTable()
                    + " ("
                    + myTable.getMeetings().getIdMeetings()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + myTable.getMeetings().getNameMeetings()
                    + " TEXT, "
                    + myTable.getMeetings().getDateMeetings()
                    + " TEXT"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + myTable.getReminders().getNameTable()
                    + " ("
                    + myTable.getReminders().getIdReminders()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + myTable.getReminders().getNameReminders()
                    + " TEXT, "
                    + myTable.getReminders().getUrgencyReminders()
                    + " TEXT"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + myTable.getPermanentConsumption().getNameTable()
                    + " ("
                    + myTable.getPermanentConsumption().getId_Consumption()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + myTable.getPermanentConsumption().getName_Consumption()
                    + " TEXT, "
                    + myTable.getPermanentConsumption().getPrice()
                    + " REAL"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + myTable.getPermanentIncome().getTable_name()
                    + " ("
                    + myTable.getPermanentIncome().getId_Income()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + myTable.getPermanentIncome().getName_Income()
                    + " TEXT, "
                    + myTable.getPermanentIncome().getPrice()
                    + " REAL"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + myTable.getPostponing().getTable_name()
                    + " ("
                    + myTable.getPostponing().getId_Postponing()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + myTable.getPostponing().getName_postponing()
                    + " TEXT, "
                    + myTable.getPostponing().getMany_months()
                    + " INTEGER, "
                    + myTable.getPostponing().getValue_of_a_goal()
                    + " REAL,"
                    + myTable.getPostponing().getHowMany()
                    + " REAL"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + myTable.getMonths().getTable_name()
                    + " ("
                    + myTable.getMonths().getDateNow()
                    + " TEXT PRIMARY KEY UNIQUE"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + myTable.getPlannedFlow().getTable_name()
                    + " ("
                    + myTable.getPlannedFlow().getId_planned_flow()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + myTable.getPlannedFlow().getName_planned_flow()
                    + " TEXT, "
                    + myTable.getPlannedFlow().getPrice()
                    + " REAL, "
                    + myTable.getPlannedFlow().get_id_months()
                    + " TEXT, "
                    + "FOREIGN KEY (" + myTable.getPlannedFlow().get_id_months() + ") "
                    + "REFERENCES " + myTable.getMonths().getTable_name() + " (" + myTable.getMonths().getDateNow() + ") "
                    + "ON DELETE CASCADE ON UPDATE CASCADE"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + myTable.getIncomeNow().getTable_name()
                    + " ("
                    + myTable.getIncomeNow().getId_Income()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + myTable.getIncomeNow().getName_Income()
                    + " TEXT, "
                    + myTable.getIncomeNow().getDate_Income()
                    + " TEXT, "
                    + myTable.getIncomeNow().getPrice()
                    + " REAL, "
                    + myTable.getIncomeNow().get_id_months()
                    + " TEXT, "
                    + "FOREIGN KEY (" + myTable.getIncomeNow().get_id_months() + ") "
                    + "REFERENCES " + myTable.getMonths().getTable_name() + " (" + myTable.getMonths().getDateNow() + ") "
                    + "ON DELETE CASCADE ON UPDATE CASCADE"
                    + ");");


            db.execSQL("CREATE TABLE "
                    + myTable.getConsumptionNow().getTable_name()
                    + " ("
                    + myTable.getConsumptionNow().getId_consumption()
                    + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + myTable.getConsumptionNow().getName_consumptione()
                    + " TEXT, "
                    + myTable.getConsumptionNow().getDate_consumption()
                    + " TEXT, "
                    + myTable.getConsumptionNow().getPrice()
                    + " REAL, "
                    + myTable.getConsumptionNow().get_id_months()
                    + " TEXT, "
                    + "FOREIGN KEY (" + myTable.getConsumptionNow().get_id_months() + ") "
                    + "REFERENCES " + myTable.getMonths().getTable_name() + " (" + myTable.getMonths().getDateNow() + ") "
                    + "ON DELETE CASCADE ON UPDATE CASCADE"
                    + ");");


        } catch (SQLException e) {
            Log.d("MyTest", "Eror create db");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //здесь изменения версии
    }


    public MyTable getMyTable() {
        return myTable;
    }

}
