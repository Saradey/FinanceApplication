package financeapplication.android8.sarad.financeapplication.FunctionalDb;


import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import financeapplication.android8.sarad.financeapplication.DatabaseEntity.ConsumptionEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.ConsumptionNowEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.Entity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.IncomeEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.IncomeNowEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.MeetingsEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.PlannedFlowEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.PostponingEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.RemindersEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.User;
import financeapplication.android8.sarad.financeapplication.Table.MyTable;
import financeapplication.android8.sarad.financeapplication.functional.SingletonUser;

public class NoteDataSource {

    private static final String PATTERN_DATE = "MMM yyyy";
    private static final String PATTERN_DATE_NOW = "dd MMM hh:mm:ss a";

    private DatabaseHelper dbHelper;
    private SQLiteDatabase database;
    private MyTable myTable;
    private SharedPreferences sharedPreferences;

    public NoteDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
        myTable = dbHelper.getMyTable();
        open();
    }


    public void addUser(User userLogNow) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getUser().getColumnId(), userLogNow.getId());   //добавили значение
        values.put(myTable.getUser().getAllMoney(), userLogNow.getAllMoney());

        database.insert(myTable.getUser().getNameTable(), null, values);
    }


    private void addMonth() {
        String str = getDateNow();

        database.execSQL("INSERT OR IGNORE INTO "
                + myTable.getMonths().getTable_name()
                + " (" + myTable.getMonths().getDateNow() + ") "
                + "VALUES "
                + "('" + str + "');");
    }


    private String getDateNow() {
        SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_DATE);
        Date currentDate = new Date();
        return sdf.format(currentDate);
    }


    private String getDateIncomeAndConsumption() {
        SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_DATE_NOW);
        Date currentDate = new Date();
        return sdf.format(currentDate);
    }


    public void open() {
        database = dbHelper.getWritableDatabase();
        addMonth();
    }


    public void close() {
        dbHelper.close();
    }


    //получаем все встречи
    public List<Entity> getAllMeetings() {
        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                myTable.getMeetings().getIdMeetings(),
                myTable.getMeetings().getNameMeetings(),
                myTable.getMeetings().getDateMeetings()
        };
        Cursor cursor = database.query(myTable.getMeetings().getNameTable(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Entity note = cursorToMeetings(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }
        cursor.close();
        return listEntity;
    }


    //добавляем встречу
    public void addMeeting(MeetingsEntity meetingsEntity) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getMeetings().getNameMeetings(), meetingsEntity.getName_meetings());   //добавили значение
        values.put(myTable.getMeetings().getDateMeetings(), meetingsEntity.getDate_meetings());

        database.insert(myTable.getMeetings().getNameTable(), null, values);  //DatabaseHelper.TABLE_NOTES название таблицы
    }


    public void updateMeeting(MeetingsEntity meetingsEntity) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getMeetings().getNameMeetings(), meetingsEntity.getName_meetings());   //добавили значение
        values.put(myTable.getMeetings().getDateMeetings(), meetingsEntity.getDate_meetings());

        database.update(myTable.getMeetings().getNameTable(),
                values,
                myTable.getMeetings().getIdMeetings() + "=" + meetingsEntity.getId(),
                null);
    }


    //удаляем встречу
    public void deleteMeetings(Entity meetingsEntity) {
        int id = meetingsEntity.getId();
        database.delete(myTable.getMeetings().getNameTable(), myTable.getMeetings().getIdMeetings()
                + " = " + id, null);
    }


    //функция создания класс встреч для отображения
    private Entity cursorToMeetings(Cursor cursor) {
        MeetingsEntity entity = new MeetingsEntity();
        entity.setId(cursor.getInt(0));
        entity.setName_meetings(cursor.getString(1));
        entity.setDate_meetings(cursor.getString(2));
        return entity;
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

    //получаем все напоминания
    public List<Entity> getAllReminders() {
        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                myTable.getReminders().getIdReminders(),
                myTable.getReminders().getNameReminders(),
                myTable.getReminders().getUrgencyReminders()
        };

        Cursor cursor = database.query(myTable.getReminders().getNameTable(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToReminders(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }
        cursor.close();
        return listEntity;
    }


    //функция создания класс напоминаний для отображения
    private Entity cursorToReminders(Cursor cursor) {
        RemindersEntity entity = new RemindersEntity();
        entity.setId(cursor.getInt(0));
        entity.setName_reminders(cursor.getString(1));
        entity.setUrgency(cursor.getString(2));
        return entity;
    }


    //добавляем напоминания
    public void addReminders(RemindersEntity reminders) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getReminders().getNameReminders(), reminders.getName_reminders());   //добавили значение
        values.put(myTable.getReminders().getUrgencyReminders(), reminders.getUrgency());

        database.insert(myTable.getReminders().getNameTable(), null, values);  //DatabaseHelper.TABLE_NOTES название таблицы
    }


    //удаляем напоминания
    public void deleteReminders(Entity reminders) {
        int id = reminders.getId();
        database.delete(myTable.getReminders().getNameTable(), myTable.getReminders().getIdReminders()
                + " = " + id, null);
    }


    public void updateReminders(RemindersEntity entity) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getReminders().getNameReminders(), entity.getName_reminders());   //добавили значение
        values.put(myTable.getReminders().getUrgencyReminders(), entity.getUrgency());

        database.update(myTable.getReminders().getNameTable(),
                values,
                myTable.getReminders().getIdReminders() + "=" + entity.getId(),
                null);
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


    public List<Entity> getAllConsumption() {
        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                myTable.getPermanentConsumption().getId_Consumption(),
                myTable.getPermanentConsumption().getName_Consumption(),
                myTable.getPermanentConsumption().getPrice()
        };

        Cursor cursor = database.query(myTable.getPermanentConsumption().getNameTable(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToConsumption(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }
        cursor.close();
        return listEntity;
    }


    private Entity cursorToConsumption(Cursor cursor) {
        ConsumptionEntity entity = new ConsumptionEntity();
        entity.setId(cursor.getInt(0));
        entity.setName_Consumption(cursor.getString(1));
        entity.setPrice(cursor.getDouble(2));
        return entity;
    }


    public void addConsumption(ConsumptionEntity consumption) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getPermanentConsumption().getName_Consumption(), consumption.getName_Consumption());   //добавили значение
        values.put(myTable.getPermanentConsumption().getPrice(), consumption.getPrice());

        database.insert(myTable.getPermanentConsumption().getNameTable(), null, values);  //DatabaseHelper.TABLE_NOTES название таблицы
    }


    public void deleteConsumption(Entity reminders) {
        int id = reminders.getId();
        database.delete(myTable.getPermanentConsumption().getNameTable(), myTable.getPermanentConsumption().getId_Consumption()
                + " = " + id, null);
    }


    public void updateConsumption(ConsumptionEntity entity) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getPermanentConsumption().getName_Consumption(), entity.getName_Consumption());   //добавили значение
        values.put(myTable.getPermanentConsumption().getPrice(), entity.getPrice());

        database.update(myTable.getPermanentConsumption().getNameTable(),
                values,
                myTable.getPermanentConsumption().getId_Consumption() + "=" + entity.getId(),
                null);
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


    public List<Entity> getAllIncome() {
        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                myTable.getPermanentIncome().getId_Income(),
                myTable.getPermanentIncome().getName_Income(),
                myTable.getPermanentIncome().getPrice()
        };

        Cursor cursor = database.query(myTable.getPermanentIncome().getTable_name(),
                allColumn, null, null, null, null, null);      //null, null, null это различные правила вывода
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToIncome(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }
        cursor.close();
        return listEntity;
    }


    private Entity cursorToIncome(Cursor cursor) {
        IncomeEntity entity = new IncomeEntity();
        entity.setId(cursor.getInt(0));
        entity.setName_Income(cursor.getString(1));
        entity.setPrice(cursor.getDouble(2));
        return entity;
    }


    public void addIncome(IncomeEntity income) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getPermanentIncome().getName_Income(), income.getName_Income());   //добавили значение
        values.put(myTable.getPermanentIncome().getPrice(), income.getPrice());

        database.insert(myTable.getPermanentIncome().getTable_name(), null, values);  //DatabaseHelper.TABLE_NOTES название таблицы
    }


    public void deleteIncome(Entity reminders) {
        int id = reminders.getId();
        database.delete(myTable.getPermanentIncome().getTable_name(), myTable.getPermanentIncome().getId_Income()
                + " = " + id, null);
    }


    public void updateIncome(IncomeEntity entity) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getPermanentIncome().getName_Income(), entity.getName_Income());   //добавили значение
        values.put(myTable.getPermanentIncome().getPrice(), entity.getPrice());

        database.update(myTable.getPermanentIncome().getTable_name(),
                values,
                myTable.getPermanentIncome().getId_Income() + "=" + entity.getId(),
                null);
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

    public List<Entity> getAllPostponing() {
        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                myTable.getPostponing().getId_Postponing(),
                myTable.getPostponing().getName_postponing(),
                myTable.getPostponing().getMany_months(),
                myTable.getPostponing().getValue_of_a_goal()
        };

        Cursor cursor = database.query(myTable.getPostponing().getTable_name(),
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToPostponing(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();
        return listEntity;
    }


    private Entity cursorToPostponing(Cursor cursor) {
        PostponingEntity entity = new PostponingEntity();
        entity.setId(cursor.getInt(0));
        entity.setName_postponing(cursor.getString(1));
        entity.setMany_months(cursor.getInt(2));
        entity.setValue_of_a_goal(cursor.getDouble(3));
        entity.calculatingPostponing();
        return entity;
    }


    public void addPostponing(PostponingEntity postponingEntity) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getPostponing().getName_postponing(), postponingEntity.getName_postponing());   //добавили значение
        values.put(myTable.getPostponing().getMany_months(), postponingEntity.getMany_months());
        values.put(myTable.getPostponing().getValue_of_a_goal(), postponingEntity.getValue_of_a_goal());
        values.put(myTable.getPostponing().getHowMany(), postponingEntity.getResult());

        database.insert(myTable.getPostponing().getTable_name(), null, values);  //DatabaseHelper.TABLE_NOTES название таблицы
    }


    public void updatePostponing(PostponingEntity entity) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getPostponing().getName_postponing(), entity.getName_postponing());   //добавили значение
        values.put(myTable.getPostponing().getMany_months(), entity.getMany_months());
        values.put(myTable.getPostponing().getValue_of_a_goal(), entity.getValue_of_a_goal());

        database.update(myTable.getPostponing().getTable_name(),
                values,
                myTable.getPostponing().getId_Postponing() + "=" + entity.getId(),
                null);
    }


    public void deletePostponing(Entity reminders) {
        int id = reminders.getId();
        database.delete(myTable.getPostponing().getTable_name(), myTable.getPostponing().getId_Postponing()
                + " = " + id, null);
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

    public List<Entity> getAllPlannedFlow() {
        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                myTable.getPlannedFlow().getId_planned_flow(),
                myTable.getPlannedFlow().getName_planned_flow(),
                myTable.getPlannedFlow().getPrice()
        };

        Cursor cursor = database.query(myTable.getPlannedFlow().getTable_name(),
                allColumn, "_id_months=?", new String[]{getDateNow()}, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToPlannedFlow(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }
        cursor.close();
        return listEntity;
    }


    private Entity cursorToPlannedFlow(Cursor cursor) {
        PlannedFlowEntity entity = new PlannedFlowEntity();
        entity.setId(cursor.getInt(0));
        entity.setName_planned_flow(cursor.getString(1));
        entity.setPrice(cursor.getDouble(2));
        return entity;
    }


    public void addPlannedFlow(PlannedFlowEntity plannedFlowEntity) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getPlannedFlow().getName_planned_flow(), plannedFlowEntity.getName_planned_flow());   //добавили значение
        values.put(myTable.getPlannedFlow().getPrice(), plannedFlowEntity.getPRICE());
        values.put(myTable.getPlannedFlow().get_id_months(), getDateNow());

        database.insert(myTable.getPlannedFlow().getTable_name(), null, values);  //DatabaseHelper.TABLE_NOTES название таблицы
    }


    public void updatePlannedFlow(PlannedFlowEntity entity) {
        ContentValues values = new ContentValues(); //это обычный хэш мап
        values.put(myTable.getPlannedFlow().getName_planned_flow(), entity.getName_planned_flow());   //добавили значение
        values.put(myTable.getPlannedFlow().getPrice(), entity.getPRICE());
        values.put(myTable.getPlannedFlow().get_id_months(), getDateNow());

        database.update(myTable.getPlannedFlow().getTable_name(),
                values,
                myTable.getPlannedFlow().getId_planned_flow() + "=" + entity.getId(),
                null);
    }


    public void deletePlannedFlow(Entity reminders) {
        int id = reminders.getId();
        database.delete(myTable.getPlannedFlow().getTable_name(), myTable.getPlannedFlow().getId_planned_flow()
                + " = " + id, null);
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

    public List<Entity> getAllIncomeNow() {
        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                myTable.getIncomeNow().getId_Income(),
                myTable.getIncomeNow().getName_Income(),
                myTable.getIncomeNow().getDate_Income(),
                myTable.getIncomeNow().getPrice()
        };

        Cursor cursor = database.query(myTable.getIncomeNow().getTable_name(),
                allColumn, "_id_months=?", new String[]{getDateNow()}, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorToIncomeNow(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }
        cursor.close();
        return listEntity;
    }


    private Entity cursorToIncomeNow(Cursor cursor) {
        IncomeNowEntity entity = new IncomeNowEntity();
        entity.setId(cursor.getInt(0));
        entity.setName_Income(cursor.getString(1));
        entity.set_date(cursor.getString(2));
        entity.setPrice(cursor.getDouble(3));
        return entity;
    }


    public void addIncomeNow(IncomeNowEntity incomeNow) {

        ContentValues values = new ContentValues();
        values.put(myTable.getIncomeNow().getName_Income(), incomeNow.getName_Income());
        values.put(myTable.getIncomeNow().getPrice(), incomeNow.getPrice());
        values.put(myTable.getIncomeNow().getDate_Income(), getDateIncomeAndConsumption());
        values.put(myTable.getIncomeNow().get_id_months(), getDateNow());
        insetAllMoneyDB(incomeNow.getPrice());
        database.insert(myTable.getIncomeNow().getTable_name(), null, values);
    }


    public void updateIncomeNow(IncomeNowEntity entity) {
        ContentValues values = new ContentValues();
        values.put(myTable.getIncomeNow().getName_Income(), entity.getName_Income());
        values.put(myTable.getIncomeNow().getPrice(), entity.getPrice());
        values.put(myTable.getIncomeNow().getDate_Income(), getDateIncomeAndConsumption());
        values.put(myTable.getIncomeNow().get_id_months(), getDateNow());

        updateIncomeDB(entity.getPrice(), entity.getId());
        database.update(myTable.getIncomeNow().getTable_name(),
                values,
                myTable.getIncomeNow().getId_Income() + "=" + entity.getId(),
                null);
    }


    public void deleteIncomeNow(Entity reminders) {
        int id = reminders.getId();
        IncomeNowEntity teamp = (IncomeNowEntity) reminders;
        deleteIncomeDB(teamp.getPrice());
        database.delete(myTable.getIncomeNow().getTable_name(), myTable.getIncomeNow().getId_Income()
                + " = " + id, null);
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


    public List<Entity> getAllConsumptionNowEntity() {
        List<Entity> listEntity = new ArrayList<>();
        String[] allColumn = {
                myTable.getConsumptionNow().getId_consumption(),
                myTable.getConsumptionNow().getName_consumptione(),
                myTable.getConsumptionNow().getDate_consumption(),
                myTable.getConsumptionNow().getPrice()
        };

        Cursor cursor = database.query(myTable.getConsumptionNow().getTable_name(),
                allColumn, "_id_months=?", new String[]{getDateNow()}, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Entity note = cursorConsumptionNowEntity(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }
        cursor.close();
        return listEntity;
    }


    private Entity cursorConsumptionNowEntity(Cursor cursor) {
        ConsumptionNowEntity entity = new ConsumptionNowEntity();
        entity.setId(cursor.getInt(0));
        entity.setName_consumptione(cursor.getString(1));
        entity.setDate_consumption(cursor.getString(2));
        entity.setPrice(cursor.getDouble(3));
        return entity;
    }


    public void addConsumptionNowEntity(ConsumptionNowEntity consumptionNowEntity) {

        ContentValues values = new ContentValues();
        values.put(myTable.getConsumptionNow().getName_consumptione(), consumptionNowEntity.getName_consumptione());
        values.put(myTable.getConsumptionNow().getPrice(), consumptionNowEntity.getPrice());
        values.put(myTable.getConsumptionNow().getDate_consumption(), getDateIncomeAndConsumption());
        values.put(myTable.getConsumptionNow().get_id_months(), getDateNow());

        insetAllMoneyDBConsumption(consumptionNowEntity.getPrice());

        database.insert(myTable.getConsumptionNow().getTable_name(), null, values);
    }


    public void updateConsumptionNowEntity(ConsumptionNowEntity entity) {
        ContentValues values = new ContentValues();
        values.put(myTable.getConsumptionNow().getName_consumptione(), entity.getName_consumptione());
        values.put(myTable.getConsumptionNow().getPrice(), entity.getPrice());
        values.put(myTable.getConsumptionNow().getDate_consumption(), getDateIncomeAndConsumption());
        values.put(myTable.getConsumptionNow().get_id_months(), getDateNow());

        updateIncomeDBConsumption(entity.getPrice(), entity.getId());

        database.update(myTable.getConsumptionNow().getTable_name(),
                values,
                myTable.getConsumptionNow().getId_consumption() + "=" + entity.getId(),
                null);
    }


    public void deleteConsumptionNowEntity(Entity reminders) {
        int id = reminders.getId();
        ConsumptionNowEntity consumptionNowEntity = (ConsumptionNowEntity) reminders;
        deleteIncomeDBConsumption(consumptionNowEntity.getPrice());
        database.delete(myTable.getConsumptionNow().getTable_name(), myTable.getConsumptionNow().getId_consumption()
                + " = " + id, null);
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

    public Double getYourSumm() {

        String[] allColumn = {
                myTable.getUser().getAllMoney(),
        };

        String id_user = String.valueOf(SingletonUser.getInstance().getId_user());

        Cursor cursor = database.query(myTable.getUser().getNameTable(),
                allColumn, "id_user=?", new String[]{id_user}, null, null, null);
        cursor.moveToFirst();
        Double teamp = cursor.getDouble(0);
        cursor.close();

        return teamp;
    }


    public Double getMonthlyPermanentIncome() {

        String[] allColumn = {
                "sum(" + myTable.getPermanentIncome().getPrice() + ")"
        };

        Cursor cursor = database.query(myTable.getPermanentIncome().getTable_name(),
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();
        Double teamp = cursor.getDouble(0);
        cursor.close();

        return teamp;
    }


    public Double getmonthlyConstantConsumption() {

        String[] allColumn = {
                "sum(" + myTable.getPermanentConsumption().getPrice() + ")"
        };

        Cursor cursor = database.query(myTable.getPermanentConsumption().getNameTable(),
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();
        Double teamp = cursor.getDouble(0);
        cursor.close();

        return teamp;
    }


    public Double getPostponing() {

        String[] allColumn = {
                "sum(" + myTable.getPostponing().getHowMany() + ")"
        };

        Cursor cursor = database.query(myTable.getPostponing().getTable_name(),
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();
        Double teamp = cursor.getDouble(0);
        cursor.close();

        return teamp;
    }


    public Double getPlanned_flow() {

        String[] allColumn = {
                "sum(" + myTable.getPlannedFlow().getPrice() + ")"
        };

        Cursor cursor = database.query(myTable.getPlannedFlow().getTable_name(),
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();
        Double teamp = cursor.getDouble(0);
        cursor.close();

        return teamp;
    }


    public Double getIncome() {

        String[] allColumn = {
                "sum(" + myTable.getIncomeNow().getPrice() + ")"
        };

        Cursor cursor = database.query(myTable.getIncomeNow().getTable_name(),
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();
        Double teamp = cursor.getDouble(0);
        cursor.close();

        return teamp;
    }


    public Double getConsumption() {

        String[] allColumn = {
                "sum(" + myTable.getConsumptionNow().getPrice() + ")"
        };

        Cursor cursor = database.query(myTable.getConsumptionNow().getTable_name(),
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();
        Double teamp = cursor.getDouble(0);
        cursor.close();

        return teamp;
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


    private void insetAllMoneyDB(double price) {
        double many = getYourSumm();
        many += price;

        ContentValues values = new ContentValues();
        values.put(myTable.getUser().getAllMoney(), many);

        int _id_user = SingletonUser.getInstance().getId_user();

        database.update(myTable.getUser().getNameTable(),
                values,
                myTable.getUser().getColumnId() + "=" + _id_user,
                null);
    }


    private void deleteIncomeDB(double price) {
        double many = getYourSumm();
        many -= price;

        ContentValues values = new ContentValues();
        values.put(myTable.getUser().getAllMoney(), many);

        int _id_user = SingletonUser.getInstance().getId_user();

        database.update(myTable.getUser().getNameTable(),
                values,
                myTable.getUser().getColumnId() + "=" + _id_user,
                null);
    }


    private void updateIncomeDB(double price, int id) {
        double many = getYourSumm();


        String[] allColumn = {
                myTable.getIncomeNow().getPrice()
        };

        Cursor cursor = database.query(myTable.getIncomeNow().getTable_name(),
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();
        Double teamp = cursor.getDouble(0);
        cursor.close();
        many -= teamp;
        many += price;

        ContentValues values = new ContentValues();
        values.put(myTable.getUser().getAllMoney(), many);

        int _id_user = SingletonUser.getInstance().getId_user();

        database.update(myTable.getUser().getNameTable(),
                values,
                myTable.getUser().getColumnId() + "=" + _id_user,
                null);
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void insetAllMoneyDBConsumption(double price) {
        double many = getYourSumm();
        many -= price;

        ContentValues values = new ContentValues();
        values.put(myTable.getUser().getAllMoney(), many);

        int _id_user = SingletonUser.getInstance().getId_user();

        database.update(myTable.getUser().getNameTable(),
                values,
                myTable.getUser().getColumnId() + "=" + _id_user,
                null);
    }


    private void deleteIncomeDBConsumption(double price) {
        double many = getYourSumm();
        many += price;

        ContentValues values = new ContentValues();
        values.put(myTable.getUser().getAllMoney(), many);

        int _id_user = SingletonUser.getInstance().getId_user();

        database.update(myTable.getUser().getNameTable(),
                values,
                myTable.getUser().getColumnId() + "=" + _id_user,
                null);
    }


    private void updateIncomeDBConsumption(double price, int id) {
        double many = getYourSumm();

        String[] allColumn = {
                myTable.getConsumptionNow().getPrice()
        };

        Cursor cursor = database.query(myTable.getConsumptionNow().getTable_name(),
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();
        Double teamp = cursor.getDouble(0);
        cursor.close();

        many += teamp;
        many -= price;

        ContentValues values = new ContentValues();
        values.put(myTable.getUser().getAllMoney(), many);

        int _id_user = SingletonUser.getInstance().getId_user();

        database.update(myTable.getUser().getNameTable(),
                values,
                myTable.getUser().getColumnId() + "=" + _id_user,
                null);
    }


}

