package financeapplication.android8.sarad.financeapplication.MyFragment;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import financeapplication.android8.sarad.financeapplication.FunctionalDb.NoteDataSource;
import financeapplication.android8.sarad.financeapplication.R;

public class FragmentAllInformation extends Fragment {


    private final Handler handler = new Handler();

    @BindView(R.id.text_view_allInf_2)
    TextView text_view_allInf_2;

    @BindView(R.id.text_view_allInf_4)
    TextView text_view_allInf_4;

    @BindView(R.id.text_view_allInf_6)
    TextView text_view_allInf_6;

    @BindView(R.id.text_view_allInf_8)
    TextView text_view_allInf_8;

    @BindView(R.id.text_view_allInf_10)
    TextView text_view_allInf_10;

    @BindView(R.id.text_view_allInf_12)
    TextView text_view_allInf_12;

    @BindView(R.id.text_view_allInf_14)
    TextView text_view_allInf_14;

    NoteDataSource noteDataSource;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_all_information, container, false);

        ButterKnife.bind(this, view);

        initDataSource();
        initUI();

        return view;
    }


    private void initDataSource() {
        noteDataSource = new NoteDataSource(getActivity());
    }

    //если повернули экран
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }


    private void initUI() {

        handler.post(new Runnable() {
            @Override
            public void run() {
                text_view_allInf_2.setText(String.valueOf(noteDataSource.getYourSumm()));
                text_view_allInf_4.setText(String.valueOf(noteDataSource.getMonthlyPermanentIncome()));
                text_view_allInf_6.setText(String.valueOf(noteDataSource.getmonthlyConstantConsumption()));
                text_view_allInf_8.setText(String.valueOf(noteDataSource.getPostponing()));
                text_view_allInf_10.setText(String.valueOf(noteDataSource.getPlanned_flow()));
                text_view_allInf_12.setText(String.valueOf(noteDataSource.getConsumption()));
                text_view_allInf_14.setText(String.valueOf(noteDataSource.getIncome()));
            }
        });

    }


}
