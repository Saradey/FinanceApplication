package financeapplication.android8.sarad.financeapplication.MyFragment;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import financeapplication.android8.sarad.financeapplication.ActivityNavigationDrawer;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.ConsumptionEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.ConsumptionNowEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.Entity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.IncomeEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.IncomeNowEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.MeetingsEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.PlannedFlowEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.PostponingEntity;
import financeapplication.android8.sarad.financeapplication.DatabaseEntity.RemindersEntity;
import financeapplication.android8.sarad.financeapplication.FunctionalDb.NoteDataSource;
import financeapplication.android8.sarad.financeapplication.R;


public class FragmentMain extends Fragment {

    private static final String INDEX_SAVE = "index";
    @BindView(R.id.list)
    ListView listView;
    private ArrayAdapter<Entity> arrayAdapter;
    private List<Entity> myStoryList;
    private int index = 0;
    private NoteDataSource noteDataSource;
    private Handler handler;
    private boolean isUpdate = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_main, container, false);

        ButterKnife.bind(this, view);

        loadSaveindex(savedInstanceState);

        handler = new Handler(getActivity().getBaseContext().getMainLooper());

        initList();

        return view;
    }


    private void loadSaveindex(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            index = savedInstanceState.getInt(INDEX_SAVE, 1);
            ActivityNavigationDrawer teamp = (ActivityNavigationDrawer) getActivity();
            noteDataSource = teamp.getNoteDataSource();
            String date;
            switch (index) {
                case 1:
                    date = getString(R.string.list_meetings) + teamp.getDateNow();
                    teamp.setTitle(date);
                    break;

                case 2:
                    date = getString(R.string.reminders) + teamp.getDateNow();
                    teamp.setTitle(date);
                    break;

                case 3:
                    date = getString(R.string.consumption) + teamp.getDateNow();
                    teamp.setTitle(date);
                    break;

                case 4:
                    date = getString(R.string.income) + teamp.getDateNow();
                    teamp.setTitle(date);
                    break;

                case 5:
                    date = getString(R.string.postponing) + teamp.getDateNow();
                    teamp.setTitle(date);
                    break;

                case 6:
                    date = getString(R.string.planned_flow) + teamp.getDateNow();
                    teamp.setTitle(date);
                    break;

                case 7:
                    date = getString(R.string.income_now) + teamp.getDateNow();
                    teamp.setTitle(date);
                    break;

                case 8:
                    date = getString(R.string.consumption_now) + teamp.getDateNow();
                    teamp.setTitle(date);
                    break;
            }
        }
    }


    //создаем контекстное меню
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }


    //если повернули экран
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(INDEX_SAVE, index);
    }


    //нажатие на контексное меню
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.menu_edit:
                isUpdate = true;
                updateElement(info.position);
                return true;
            case R.id.menu_delete:
                deleteElement(info.position);
                return true;
        }
        return super.onContextItemSelected(item);
    }


    //обновления элемента
    private void updateElement(int id) {
        switch (index) {
            case 1:
                updateMeetings(id);
                break;

            case 2:
                updateReminders(id);
                break;

            case 3:
                updateConsumption(id);
                break;

            case 4:
                updateIncome(id);
                break;

            case 5:
                updatePostponing(id);
                break;

            case 6:
                updatePlannedFlow(id);
                break;

            case 7:
                updateIncomeNowEntity(id);
                break;

            case 8:
                updateConsumptionNowEntity(id);
                break;
        }
    }


    private void updateConsumptionNowEntity(int id) {
        DialogWindowAddConsumptionNowEntity(id);
        dataUpdatedConsumptionNowEntity();
    }


    private void updateIncomeNowEntity(int id) {
        DialogWindowAddIncomeNowEntity(id);
        dataUpdatedIncomeNowEntity();
    }


    private void updatePlannedFlow(int id) {
        DialogWindowAddPlannedFlow(id);
        dataUpdatedPlannedFlow();
    }


    private void updatePostponing(int id) {
        DialogWindowAddPostponing(id);
        dataUpdatedPostponing();
    }


    private void updateIncome(int id) {
        DialogWindowAddIncome(id);
        dataUpdatedIncome();
    }


    private void updateConsumption(int id) {
        DialogWindowAddConsumption(id);
        dataUpdatedConsumption();
    }


    private void updateReminders(int id) {
        DialogWindowAddReminders(id);
        dataUpdatedReminders();
    }


    private void updateMeetings(int id) {
        DialogWindowAddMeetings(id);
        dataUpdatedMeetings();
    }


    //удаление элементов
    private void deleteElement(int id) {
        switch (index) {
            case 1:
                deleteMeetings(id);
                break;

            case 2:
                deleteReminders(id);
                break;

            case 3:
                deleteConsumption(id);
                break;

            case 4:
                deleteIncome(id);
                break;

            case 5:
                deletePostponing(id);
                break;

            case 6:
                deletePlannedFlow(id);
                break;

            case 7:
                deleteIncomeNowEntity(id);
                break;

            case 8:
                deleteConsumptionNowEntity(id);
                break;
        }
    }

    private void deleteMeetings(int id) {
        noteDataSource.deleteMeetings(myStoryList.get(id));
        dataUpdatedMeetings();
    }


    //иницилизация списка по индексу
    private void initList() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                switch (index) {
                    case 0:
                        break;
                    case 1:
                        initListMeetings();
                        break;

                    case 2:
                        initListReminders();
                        break;

                    case 3:
                        initListConsumption();
                        break;

                    case 4:
                        initListIncome();
                        break;

                    case 5:
                        initListPostponing();
                        break;

                    case 6:
                        initListPlannedFlow();
                        break;

                    case 7:
                        initListIncomeNowEntity();
                        break;

                    case 8:
                        initListConsumptionNowEntity();
                        break;
                }
            }
        }).start();
    }


    //иницилизация списка встреч
    private void initListMeetings() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                myStoryList = noteDataSource.getAllMeetings();
                arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, myStoryList);
                listView.setAdapter(arrayAdapter);
                registerForContextMenu(listView);
            }
        });
    }


    public void DialogWindowAddMeetings(int id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        final int MyId = id;

        final EditText nameBox = new EditText(getActivity());
        nameBox.setHint(getString(R.string.dialog_add_meetings));
        layout.addView(nameBox);

        final EditText dateBox = new EditText(getActivity());
        dateBox.setHint(getString(R.string.dialog_add_meetings_2));
        layout.addView(dateBox);
        dialog.setView(layout);

        dialog.setNegativeButton(R.string.negativ_draw_text, null);

        if (!isUpdate) {
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    MeetingsEntity entity = new MeetingsEntity();
                    String name = nameBox.getText().toString();
                    String _date = dateBox.getText().toString();

                    if (!name.equals("") && !_date.equals("")) {
                        entity.setName_meetings(name);
                        entity.setDate_meetings(_date);
                        noteDataSource.addMeeting(entity);
                        dataUpdatedMeetings();
                    }
                }
            });
        } else {
            isUpdate = false;
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    MeetingsEntity entity = new MeetingsEntity();
                    String name = nameBox.getText().toString();
                    String _date = dateBox.getText().toString();

                    if (!name.equals("") && !_date.equals("")) {
                        entity.setName_meetings(name);
                        entity.setDate_meetings(_date);
                        entity.setId(myStoryList.get(MyId).getId());
                        noteDataSource.updateMeeting(entity);
                        dataUpdatedMeetings();
                    }
                }
            });
        }
        dialog.show();
    }

    //получаем ссылку на класс базы данных
    public void setDataSource(NoteDataSource noteDataSource) {
        this.noteDataSource = noteDataSource;
    }


    //ловим индекс нажатия
    public void setIndexDatabase(int index) {
        this.index = index;
    }


    //обновление списка встреч
    public void dataUpdatedMeetings() {
        myStoryList.clear();
        myStoryList.addAll(noteDataSource.getAllMeetings());
        arrayAdapter.notifyDataSetChanged(); //просим перерисовать данные
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void initListReminders() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                myStoryList = noteDataSource.getAllReminders();
                arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, myStoryList);
                listView.setAdapter(arrayAdapter);
                registerForContextMenu(listView);
            }
        });
    }


    public void DialogWindowAddReminders(int id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        final int MyId = id;

        final EditText nameBox = new EditText(getActivity());
        nameBox.setHint(getString(R.string.reminders_title_1));
        layout.addView(nameBox);

        final EditText urgencyBox = new EditText(getActivity());
        urgencyBox.setHint(getString(R.string.reminders_title_2));
        layout.addView(urgencyBox);
        dialog.setView(layout);
        dialog.setNegativeButton(R.string.negativ_draw_text, null);

        if (!isUpdate) {
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    RemindersEntity entity = new RemindersEntity();
                    String name = nameBox.getText().toString();
                    String urgency = urgencyBox.getText().toString();

                    if (!name.equals("") && !urgency.equals("")) {
                        entity.setName_reminders(name);
                        entity.setUrgency(urgency);
                        noteDataSource.addReminders(entity);
                        dataUpdatedReminders();
                    }
                }
            });
        } else {
            isUpdate = false;
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    RemindersEntity entity = new RemindersEntity();
                    String name = nameBox.getText().toString();
                    String urgency = urgencyBox.getText().toString();

                    if (!name.equals("") && !urgency.equals("")) {
                        entity.setName_reminders(name);
                        entity.setUrgency(urgency);
                        entity.setId(myStoryList.get(MyId).getId());
                        noteDataSource.updateReminders(entity);
                        dataUpdatedReminders();
                    }
                }
            });
        }
        dialog.show();
    }


    public void dataUpdatedReminders() {
        myStoryList.clear();
        myStoryList.addAll(noteDataSource.getAllReminders());
        arrayAdapter.notifyDataSetChanged(); //просим перерисовать данные
    }

    private void deleteReminders(int id) {
        noteDataSource.deleteReminders(myStoryList.get(id));
        dataUpdatedReminders();
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


    private void initListConsumption() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                myStoryList = noteDataSource.getAllConsumption();
                arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, myStoryList);
                listView.setAdapter(arrayAdapter);
                registerForContextMenu(listView);
            }
        });
    }


    public void DialogWindowAddConsumption(int id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        final int MyId = id;

        final EditText nameBox = new EditText(getActivity());
        nameBox.setHint(getString(R.string.consumption_title_1));
        layout.addView(nameBox);

        final EditText priceBox = new EditText(getActivity());
        priceBox.setHint(getString(R.string.consumption_title_2));
        layout.addView(priceBox);
        dialog.setView(layout);
        dialog.setNegativeButton(R.string.negativ_draw_text, null);

        if (!isUpdate) {
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    ConsumptionEntity entity = new ConsumptionEntity();
                    String name = nameBox.getText().toString();

                    try {
                        double price = Double.parseDouble(priceBox.getText().toString());
                        price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();

                        if (price > 0 && !name.equals("")) {
                            entity.setPrice(price);
                            entity.setName_Consumption(name);
                            noteDataSource.addConsumption(entity);
                            dataUpdatedConsumption();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            isUpdate = false;
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    ConsumptionEntity entity = new ConsumptionEntity();
                    String name = nameBox.getText().toString();

                    try {
                        double price = Double.parseDouble(priceBox.getText().toString());
                        price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();

                        if (price > 0 && !name.equals("")) {
                            entity.setPrice(price);
                            entity.setName_Consumption(name);
                            entity.setId(myStoryList.get(MyId).getId());
                            noteDataSource.updateConsumption(entity);
                            dataUpdatedConsumption();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        dialog.show();
    }


    private void deleteConsumption(int id) {
        noteDataSource.deleteConsumption(myStoryList.get(id));
        dataUpdatedConsumption();
    }


    public void dataUpdatedConsumption() {
        myStoryList.clear();
        myStoryList.addAll(noteDataSource.getAllConsumption());
        arrayAdapter.notifyDataSetChanged(); //просим перерисовать данные
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


    private void initListIncome() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                myStoryList = noteDataSource.getAllIncome();
                arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, myStoryList);
                listView.setAdapter(arrayAdapter);
                registerForContextMenu(listView);
            }
        });
    }


    public void DialogWindowAddIncome(int id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        final int MyId = id;

        final EditText nameBox = new EditText(getActivity());
        nameBox.setHint(getString(R.string.income_title_1));
        layout.addView(nameBox);

        final EditText priceBox = new EditText(getActivity());
        priceBox.setHint(getString(R.string.income_title_2));
        layout.addView(priceBox);
        dialog.setView(layout);
        dialog.setNegativeButton(R.string.negativ_draw_text, null);

        if (!isUpdate) {
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    IncomeEntity entity = new IncomeEntity();
                    String name = nameBox.getText().toString();

                    try {
                        double price = Double.parseDouble(priceBox.getText().toString());
                        price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();

                        if (price > 0 && !name.equals("")) {
                            entity.setPrice(price);
                            entity.setName_Income(name);
                            noteDataSource.addIncome(entity);
                            dataUpdatedIncome();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            isUpdate = false;
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    IncomeEntity entity = new IncomeEntity();
                    String name = nameBox.getText().toString();

                    try {
                        double price = Double.parseDouble(priceBox.getText().toString());
                        price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();

                        if (price > 0 && !name.equals("")) {
                            entity.setPrice(price);
                            entity.setName_Income(name);
                            entity.setId(myStoryList.get(MyId).getId());
                            noteDataSource.updateIncome(entity);
                            dataUpdatedIncome();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        dialog.show();
    }


    public void dataUpdatedIncome() {
        myStoryList.clear();
        myStoryList.addAll(noteDataSource.getAllIncome());
        arrayAdapter.notifyDataSetChanged(); //просим перерисовать данные
    }


    private void deleteIncome(int id) {
        noteDataSource.deleteIncome(myStoryList.get(id));
        dataUpdatedIncome();
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void initListPostponing() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                myStoryList = noteDataSource.getAllPostponing();
                arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, myStoryList);
                listView.setAdapter(arrayAdapter);
                registerForContextMenu(listView);
            }
        });
    }


    public void DialogWindowAddPostponing(int id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        final int MyId = id;

        final EditText nameBox = new EditText(getActivity());
        nameBox.setHint(getString(R.string.postponing_title_1));
        layout.addView(nameBox);

        final EditText manyBox = new EditText(getActivity());
        manyBox.setHint(getString(R.string.postponing_title_3));
        layout.addView(manyBox);

        final EditText priceBox = new EditText(getActivity());
        priceBox.setHint(getString(R.string.postponing_title_2));
        layout.addView(priceBox);

        dialog.setView(layout);
        dialog.setNegativeButton(R.string.negativ_draw_text, null);
        if (!isUpdate) {
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    PostponingEntity entity = new PostponingEntity();
                    String name = nameBox.getText().toString();

                    try {
                        int many = Integer.parseInt(manyBox.getText().toString());
                        double price = Double.parseDouble(priceBox.getText().toString());
                        price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();

                        if (many > 0 && price > 0 && !name.equals("")) {
                            entity.setName_postponing(name);
                            entity.setMany_months(many);
                            entity.setValue_of_a_goal(price);
                            entity.calculatingPostponing();
                            noteDataSource.addPostponing(entity);
                            dataUpdatedPostponing();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            isUpdate = false;
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    PostponingEntity entity = new PostponingEntity();
                    String name = nameBox.getText().toString();

                    try {
                        int many = Integer.parseInt(manyBox.getText().toString());
                        double price = Double.parseDouble(priceBox.getText().toString());
                        price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();

                        if (many > 0 && price > 0 && !name.equals("")) {
                            entity.setName_postponing(name);
                            entity.setMany_months(many);
                            entity.setValue_of_a_goal(price);
                            entity.calculatingPostponing();
                            entity.setId(myStoryList.get(MyId).getId());
                            noteDataSource.updatePostponing(entity);
                            dataUpdatedPostponing();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        dialog.show();
    }


    private void deletePostponing(int id) {
        noteDataSource.deletePostponing(myStoryList.get(id));
        dataUpdatedPostponing();
    }


    public void dataUpdatedPostponing() {
        myStoryList.clear();
        myStoryList.addAll(noteDataSource.getAllPostponing());
        arrayAdapter.notifyDataSetChanged(); //просим перерисовать данные
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void initListPlannedFlow() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                myStoryList = noteDataSource.getAllPlannedFlow();
                arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, myStoryList);
                listView.setAdapter(arrayAdapter);
                registerForContextMenu(listView);
            }
        });
    }


    public void DialogWindowAddPlannedFlow(int id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        final int MyId = id;

        final EditText nameBox = new EditText(getActivity());
        nameBox.setHint(getString(R.string.planned_flow_title1));
        layout.addView(nameBox);

        final EditText priceBox = new EditText(getActivity());
        priceBox.setHint(getString(R.string.planned_flow_title2));
        layout.addView(priceBox);
        dialog.setView(layout);
        dialog.setNegativeButton(R.string.negativ_draw_text, null);

        if (!isUpdate) {
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    PlannedFlowEntity entity = new PlannedFlowEntity();
                    String name = nameBox.getText().toString();

                    try {
                        double price = Double.parseDouble(priceBox.getText().toString());
                        price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();
                        if (price > 0 && !name.equals("")) {
                            entity.setPrice(price);
                            entity.setName_planned_flow(name);
                            noteDataSource.addPlannedFlow(entity);
                            dataUpdatedPlannedFlow();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            isUpdate = false;
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    PlannedFlowEntity entity = new PlannedFlowEntity();
                    String name = nameBox.getText().toString();

                    try {
                        double price = Double.parseDouble(priceBox.getText().toString());
                        price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();
                        if (price > 0 && !name.equals("")) {
                            entity.setPrice(price);
                            entity.setName_planned_flow(name);
                            entity.setId(myStoryList.get(MyId).getId());
                            noteDataSource.updatePlannedFlow(entity);
                            dataUpdatedPlannedFlow();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        dialog.show();
    }


    public void dataUpdatedPlannedFlow() {
        myStoryList.clear();
        myStoryList.addAll(noteDataSource.getAllPlannedFlow());
        arrayAdapter.notifyDataSetChanged(); //просим перерисовать данные
    }

    private void deletePlannedFlow(int id) {
        noteDataSource.deletePlannedFlow(myStoryList.get(id));
        dataUpdatedPlannedFlow();
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


    private void initListIncomeNowEntity() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                myStoryList = noteDataSource.getAllIncomeNow();
                arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, myStoryList);
                listView.setAdapter(arrayAdapter);
                registerForContextMenu(listView);
            }
        });
    }


    public void DialogWindowAddIncomeNowEntity(int id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        final int MyId = id;

        final EditText nameBox = new EditText(getActivity());
        nameBox.setHint(getString(R.string.income_now_title_1));
        layout.addView(nameBox);

        final EditText priceBox = new EditText(getActivity());
        priceBox.setHint(getString(R.string.income_now_title_2));
        layout.addView(priceBox);


        dialog.setView(layout);
        dialog.setNegativeButton(R.string.negativ_draw_text, null);

        if (!isUpdate) {
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    IncomeNowEntity entity = new IncomeNowEntity();
                    String name = nameBox.getText().toString();
                    try {
                        double price = Double.parseDouble(priceBox.getText().toString());
                        price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();
                        if (price > 0 && !name.equals("")) {
                            entity.setPrice(price);
                            entity.setName_Income(name);
                            noteDataSource.addIncomeNow(entity);
                            dataUpdatedIncomeNowEntity();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            isUpdate = false;
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    IncomeNowEntity entity = new IncomeNowEntity();
                    String name = nameBox.getText().toString();
                    try {
                        double price = Double.parseDouble(priceBox.getText().toString());
                        price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();
                        if (price > 0 && !name.equals("")) {
                            entity.setPrice(price);
                            entity.setName_Income(name);
                            entity.setId(myStoryList.get(MyId).getId());
                            noteDataSource.updateIncomeNow(entity);
                            dataUpdatedIncomeNowEntity();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        dialog.show();
    }

    public void dataUpdatedIncomeNowEntity() {
        myStoryList.clear();
        myStoryList.addAll(noteDataSource.getAllIncomeNow());
        arrayAdapter.notifyDataSetChanged(); //просим перерисовать данные
    }


    private void deleteIncomeNowEntity(int id) {
        noteDataSource.deleteIncomeNow(myStoryList.get(id));
        dataUpdatedIncomeNowEntity();
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void initListConsumptionNowEntity() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                myStoryList = noteDataSource.getAllConsumptionNowEntity();
                arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, myStoryList);
                listView.setAdapter(arrayAdapter);
                registerForContextMenu(listView);
            }
        });
    }


    public void DialogWindowAddConsumptionNowEntity(int id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        final int MyId = id;

        final EditText nameBox = new EditText(getActivity());
        nameBox.setHint(getString(R.string.consumption_now_title_1));
        layout.addView(nameBox);

        final EditText priceBox = new EditText(getActivity());
        priceBox.setHint(getString(R.string.consumption_now_title_2));
        layout.addView(priceBox);


        dialog.setView(layout);
        dialog.setNegativeButton(R.string.negativ_draw_text, null);

        if (!isUpdate) {
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    ConsumptionNowEntity entity = new ConsumptionNowEntity();
                    String name = nameBox.getText().toString();

                    try {
                        double price = Double.parseDouble(priceBox.getText().toString());
                        price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();
                        if (price > 0 && !name.equals("")) {
                            entity.setPrice(price);
                            entity.setName_consumptione(name);
                            noteDataSource.addConsumptionNowEntity(entity);
                            dataUpdatedConsumptionNowEntity();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            isUpdate = false;
            dialog.setPositiveButton(R.string.position_draw_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    ConsumptionNowEntity entity = new ConsumptionNowEntity();
                    String name = nameBox.getText().toString();

                    try {
                        double price = Double.parseDouble(priceBox.getText().toString());
                        price = new BigDecimal(price).setScale(2, RoundingMode.UP).doubleValue();
                        if (price > 0 && !name.equals("")) {
                            entity.setPrice(price);
                            entity.setName_consumptione(name);
                            entity.setId(myStoryList.get(MyId).getId());
                            noteDataSource.updateConsumptionNowEntity(entity);
                            dataUpdatedConsumptionNowEntity();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        dialog.show();
    }


    public void dataUpdatedConsumptionNowEntity() {
        myStoryList.clear();
        myStoryList.addAll(noteDataSource.getAllConsumptionNowEntity());
        arrayAdapter.notifyDataSetChanged(); //просим перерисовать данные
    }


    private void deleteConsumptionNowEntity(int id) {
        noteDataSource.deleteConsumptionNowEntity(myStoryList.get(id));
        dataUpdatedConsumptionNowEntity();
    }

}
