package financeapplication.android8.sarad.financeapplication.Table;


public class MyTable {

    private User user = new User();
    private Meetings meetings = new Meetings();
    private Reminders reminders = new Reminders();
    private PermanentConsumption permanentConsumption = new PermanentConsumption();
    private PermanentIncome permanentIncome = new PermanentIncome();
    private Postponing postponing = new Postponing();
    private Months months = new Months();
    private PlannedFlow plannedFlow = new PlannedFlow();
    private IncomeNow incomeNow = new IncomeNow();
    private СonsumptionNow consumptionNow = new СonsumptionNow();

    public СonsumptionNow getConsumptionNow() {
        return consumptionNow;
    }

    public IncomeNow getIncomeNow() {
        return incomeNow;
    }

    public PlannedFlow getPlannedFlow() {
        return plannedFlow;
    }

    public Months getMonths() {
        return months;
    }

    public PermanentIncome getPermanentIncome() {
        return permanentIncome;
    }

    public PermanentConsumption getPermanentConsumption() {
        return permanentConsumption;
    }

    public Reminders getReminders() {
        return reminders;
    }

    public User getUser() {
        return user;
    }

    public Meetings getMeetings() {
        return meetings;
    }

    public Postponing getPostponing() {
        return postponing;
    }

    public class User {
        private final String columnId = "id_user";
        private final String allMoney = "allMoney";
        private final String NameTable = "_user";

        public String getAllMoney() {
            return allMoney;
        }

        public String getColumnId() {
            return columnId;
        }

        public String getNameTable() {
            return NameTable;
        }
    }


    public class Meetings {

        private final String columnIdMeetings = "id_meetings";
        private final String columnNameMeetings = "name_meetings";
        private final String columnDateMeetings = "date_meetings";
        private final String columnIdUser = "_id_user";
        private final String nameTable = "meetings";

        public String getIdMeetings() {
            return columnIdMeetings;
        }

        public String getNameMeetings() {
            return columnNameMeetings;
        }

        public String getDateMeetings() {
            return columnDateMeetings;
        }

        public String getIdUser() {
            return columnIdUser;
        }

        public String getNameTable() {
            return nameTable;
        }
    }


    public class Reminders {

        private final String columnIdReminders = "id_reminders";
        private final String columnNameReminders = "name_reminders";
        private final String columnUrgencyReminders = "urgency";
        private final String columnIdUser = "_id_user";
        private final String nameTable = "reminders";

        public String getIdReminders() {
            return columnIdReminders;
        }

        public String getNameReminders() {
            return columnNameReminders;
        }

        public String getUrgencyReminders() {
            return columnUrgencyReminders;
        }

        public String getIdUser() {
            return columnIdUser;
        }

        public String getNameTable() {
            return nameTable;
        }
    }


    public class PermanentConsumption {
        private final String id_Consumption = "id_Consumption";
        private final String name_Consumption = "name_Consumption";
        private final String price = "price";
        private final String _id_user = "_id_user";
        private final String nameTable = "monthlyConstantConsumption";

        public String getNameTable() {
            return nameTable;
        }

        public String getId_Consumption() {
            return id_Consumption;
        }

        public String getName_Consumption() {
            return name_Consumption;
        }

        public String getPrice() {
            return price;
        }

        public String get_id_user() {
            return _id_user;
        }
    }


    public class PermanentIncome {
        private final String id_Income = "id_Income";
        private final String name_Income = "name_Income";
        private final String price = "price";
        private final String _id_user = "_id_user";
        private final String table_name = "monthlyPermanentIncome";

        public String getTable_name() {
            return table_name;
        }

        public String getId_Income() {
            return id_Income;
        }

        public String getName_Income() {
            return name_Income;
        }

        public String getPrice() {
            return price;
        }

        public String get_id_user() {
            return _id_user;
        }
    }


    public class Postponing {
        private final String id_Postponing = "id_Postponing";
        private final String value_of_a_goal = "value_of_a_goal";
        private final String many_months = "many_months";
        private final String name_postponing = "name_postponing";
        private final String table_name = "postponing";
        private final String howMany = "how_many";

        public String getHowMany() {
            return howMany;
        }

        public String getId_Postponing() {
            return id_Postponing;
        }

        public String getValue_of_a_goal() {
            return value_of_a_goal;
        }

        public String getMany_months() {
            return many_months;
        }

        public String getName_postponing() {
            return name_postponing;
        }

        public String getTable_name() {
            return table_name;
        }
    }


    public class Months {
        private final String dateNow = "dateNow";
        private final String table_name = "months";

        public String getTable_name() {
            return table_name;
        }

        public String getDateNow() {
            return dateNow;
        }
    }


    public class PlannedFlow {
        private final String id_planned_flow = "id_planned_flow";
        private final String name_planned_flow = "name_planned_flow";
        private final String price = "price";
        private final String _id_months = "_id_months";
        private final String table_name = "PlannedFlow";

        public String getId_planned_flow() {
            return id_planned_flow;
        }

        public String getName_planned_flow() {
            return name_planned_flow;
        }

        public String getPrice() {
            return price;
        }

        public String get_id_months() {
            return _id_months;
        }

        public String getTable_name() {
            return table_name;
        }
    }


    public class IncomeNow {
        private final String id_Income = "id_Income";
        private final String name_Income = "name_Income";
        private final String date_Income = "date_Income";
        private final String price = "price";
        private final String _id_months = "_id_months";
        private final String table_name = "income";

        public String getId_Income() {
            return id_Income;
        }

        public String getName_Income() {
            return name_Income;
        }

        public String getDate_Income() {
            return date_Income;
        }

        public String getPrice() {
            return price;
        }

        public String get_id_months() {
            return _id_months;
        }

        public String getTable_name() {
            return table_name;
        }
    }


    public class СonsumptionNow {
        private final String id_consumption = "id_consumption";
        private final String name_consumptione = "name_consumptione";
        private final String date_consumption = "date_consumption";
        private final String price = "price";
        private final String _id_months = "_id_months";
        private final String table_name = "consumption";

        public String getId_consumption() {
            return id_consumption;
        }

        public String getName_consumptione() {
            return name_consumptione;
        }

        public String getDate_consumption() {
            return date_consumption;
        }

        public String getPrice() {
            return price;
        }

        public String get_id_months() {
            return _id_months;
        }

        public String getTable_name() {
            return table_name;
        }
    }


}
