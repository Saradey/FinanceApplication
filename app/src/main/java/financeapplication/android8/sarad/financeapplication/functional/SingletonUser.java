package financeapplication.android8.sarad.financeapplication.functional;


public class SingletonUser {
    private static SingletonUser instance;
    private int id_user;
    private Double allMoney;

    private SingletonUser() {
    }

    public static synchronized SingletonUser getInstance() {
        if (instance == null) {
            instance = new SingletonUser();
        }
        return instance;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public Double getAllMoney() {
        return allMoney;
    }

    public void setAllMoney(Double allMoney) {
        this.allMoney = allMoney;
    }
}
